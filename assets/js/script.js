var modelViewer = document.querySelector('model-viewer');
var arButtonEl = document.getElementById("ar-button");
var wrapperEl = document.querySelector(".c-wrapper");
var descElement = wrapperEl.offsetHeight;
var detailEl = document.querySelector("details");
var btnWrapperEl = document.querySelector(".c-button-wrapper");
var btnScanElement = btnWrapperEl.offsetHeight;
var summaryEl = document.querySelector("summary");
var descriptionEl = document.querySelector("p");
var urlParams = new URLSearchParams(window.location.search);
var object3D = urlParams.get('object') == null ? null : urlParams.get('object');

const onProgress = (event) => {
  const progressBar = event.target.querySelector('.progress-bar');
  const updatingBar = event.target.querySelector('.update-bar');
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};

modelViewer.addEventListener('progress', onProgress);
document.getElementById("ar-button").style.bottom = document.querySelector(".c-wrapper").offsetHeight - (document.querySelector(".c-button-wrapper").offsetHeight - 18) + "px";

detailEl.addEventListener("toggle", function() {
  document.getElementById("ar-button").style.bottom = document.querySelector(".c-wrapper").offsetHeight - (document.querySelector(".c-button-wrapper").offsetHeight - 18) + "px";
})

console.log(object3D)

if(object3D != null){
  modelViewer.setAttribute('src','assets/models/anatomy_'+object3D+'.glb')
  modelViewer.setAttribute('poster','assets/images/poster-'+object3D+'.webp')
  if(object3D == 'skin') {
    summaryEl.innerText = "SKIN ANATOMY"
    descriptionEl.innerText = "Skin is the largest organ in the body and covers the body's entire external surface. It is made up of three layers, the epidermis, dermis, and the hypodermis, all three of which vary significantly in their anatomy and function. The skin's structure is made up of an intricate network which serves as the body’s initial barrier against pathogens, UV light, and chemicals, and mechanical injury. It also regulates temperature and the amount of water released into the environment."
  }
}else{
  modelViewer.setAttribute('src','assets/models/anatomy_cardiac.glb')
  modelViewer.setAttribute('ios-src','assets/models/anatomy_cardiac.usdz')
}

// modelViewer.addEventListener('load', () => {
//   if(object3D != null){
//     if (modelViewer.canActivateAR) {
//       modelViewer.activateAR();
//     } else {
//       console.error('AR mode is not supported.');
//     }
//   }
// });